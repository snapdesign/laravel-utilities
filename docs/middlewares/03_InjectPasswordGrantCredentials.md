#InjectPasswordGrantCredentials Middleware

Diese Middleware fügt dem Request die OAuth-Client Daten hinzu.

##Config
Füge deiner 'config/auth.php' folgenden Eintrag hinzu:
```
return [
    //
    'password_grant_client_id' => 1, //Id deines Password Grant Client
];
```