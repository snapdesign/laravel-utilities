#Cors Middleware
Die CorsMiddleware behandelt Preflight-Requests und sendet die angegbenen Headers zurück.

##Service Provider registrieren
/config/app.php
```php
   /*
     * Package Service Providers...
     */
    Snapdesign\Utilities\Providers\CorsServiceProvider::class,
```
##Config veröffentlichen
```bash
$ php artisan vendor:publish --provider="Snapdesign\Laravel\Utilities\Providers\CorsServiceProvider"
```