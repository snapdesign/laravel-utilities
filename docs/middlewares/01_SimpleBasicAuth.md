#Simple Basic Auth
Simple Basic Auth ermöglicht eine Basic Auth authentifizierung in Laravel ohne das eine User-Datenbank benötigt wird.

##Service Provider registrieren
/config/app.php
```php
   /*
     * Package Service Providers...
     */
    Snapdesign\Utilities\Providers\SimpleBasicAuthServiceProvider::class,
```
##Config veröffentlichen
```bash
$ php artisan vendor:publish --provider="Snapdesign\Laravel\Utilities\Providers\SimpleBasicAuthServiceProvider"
```

##Benutzer und Passwort in der Environment-Datei erfassen
/.env
```
BASIC_AUTH_USER=admin
BASIC_AUTH_Password=secret
```

##Middleware benutzen

```php
Route::get('/secure', 'AppController@index')->middleware('auth.simple_basic');
```