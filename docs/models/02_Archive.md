#Archive Trait
Mit dem Archive Trait können Models ähnlich wie beim SoftDelte Archiviert werden.

##Migration
Füge deiner Migration folgende Zeile hinzu:
```
$table->timestamp('archived_at')->nullable();
```

##Model
Füge dem Model den Trait hinzu
```
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
use Snapdesign\Laravel\Utilities\Models\Archive;
 
class Project extends Model
{
    use Archive;
}
```

##Usage
```
    $model->archive();
    
    Model::withArchived()->get();
```