#Encryptable Trait
Dieser Trait ermöglicht es Attribute eines Models zu verschlüsseln

##Usage
Füge dem gewünschten Model den `Encryptable-Trait hinzu und setze die zu verschlüsselnden Attribute im $encryptable` -Array
```
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
use Snapdesign\Laravel\Utilities\Models\Encryptable;
 
class Project extends Model
{
    use Encryptable;
    
    protected $encryptable = ['name'];
}
````