#File Controller
Der FileController ermöglicht das bereitstellen von Downloads über eine Route, dies ermöglicht es Dateien über Middlewares zu schützen.

##Controller Verwenden
```php
Route::get('/file/{filename}', '\Snapdesign\Laravel\Utilities\Http\Controllers\FileController@downloadFile')->where('filename', '^[^/]+$')->middleware('auth.basic');
```