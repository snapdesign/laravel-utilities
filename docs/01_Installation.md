#Installation

##Package via composer laden.<br/><br/>
composer.json (Prod)
```
///
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:snapdesign/laravel-utilities.git"
    }
],
"require": {
    "snapdesign/laravel-utilities":"1.0.0"
},
///
```

composer.json (Dev)
```
///
"repositories": [
    {
        "type": "path",
        "url": "packages/snapdesign/laravel-utilities/"
    }
],
"require": {
    "snapdesign/laravel-utilities":"dev-master"
},
///
```