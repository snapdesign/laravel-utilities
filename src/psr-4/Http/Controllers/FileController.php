<?php

namespace Snapdesign\Laravel\Utilities\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    /**
     * Provides a file for download
     *
     * @param string $filename
     * @param string $diskName
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadFile($filename, $diskName = null){
        $diskName = $diskName ?: config('filesystems.default');
        $filename = Storage::disk($diskName)->getDriver()->getAdapter()->applyPathPrefix($filename);

        if(file_exists($filename) && is_file($filename)){
            return response()->download($filename);
        }

        abort(404);
    }
}
