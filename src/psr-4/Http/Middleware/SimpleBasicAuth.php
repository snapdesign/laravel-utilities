<?php

namespace Snapdesign\Laravel\Utilities\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class SimpleBasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = config('simple_basic_auth.user');
        $password = config('simple_basic_auth.password');

        if($request->getUser() !== $user || $request->getPassword() !== $password){
            $headers = ['WWW-Authenticate' => 'Basic'];
            return new Response('Invalid credentials.', 401, $headers);
        }

        return $next($request);
    }
}
