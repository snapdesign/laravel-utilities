<?php

namespace Snapdesign\Laravel\Utilities\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Exception;

class InjectPasswordGrantCredentials
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle($request, Closure $next)
    {
        if ($request->grant_type == 'password') {
            $client = DB::table('oauth_clients')
                ->where('id', config('auth.password_grant_client_id'))
                ->first();

            if(!$client){
                throw new Exception('OAuth-Client was not found...');
            }

            $request->request->add([
                'client_id' => $client->id,
                'client_secret' => $client->secret,
            ]);
        }

        return $next($request);
    }
}