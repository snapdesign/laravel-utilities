<?php

namespace Snapdesign\Laravel\Utilities\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $response->headers->set('Access-Control-Allow-Origin', config('cors.allowedOrigins'));
        $response->headers->set('Access-Control-Allow-Methods', config('cors.allowedMethods'));
        $response->headers->set('Access-Control-Allow-Headers', config('cors.allowedHeaders'));
        $response->headers->set('Access-Control-Allow-Credentials', config('cors.allowCredentials'));
        return $response;
    }
}
