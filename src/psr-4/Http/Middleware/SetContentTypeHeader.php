<?php

namespace Snapdesign\Laravel\Utilities\Http\Middleware;

use Closure;

class SetContentTypeHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->headers->get('content-type') === 'text/plain') {
            $request->headers->add(['content-type' => 'application/json']);
            $request->server->set('CONTENT_TYPE', 'application/json');
        }
        return $next($request);
    }
}