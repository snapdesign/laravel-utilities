<?php


namespace Snapdesign\Laravel\Utilities\Models;


use Snapdesign\Laravel\Utilities\Models\Scopes\ArchivedScope;

trait Archive
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ArchivedScope());
    }

    public function archive(){
        return $this
            ->newQueryWithoutScopes()
            ->where($this->getKeyName(), '=', $this->getKeyForSaveQuery())
            ->update([$this->getArchivedAtColumn() => $this->freshTimestampString()]);
    }

    /**
     * Determine if the model instance has been soft-deleted.
     *
     * @return bool
     */
    public function archived()
    {
        return ! is_null($this->{$this->getArchivedAtColumn()});
    }

    /**
     * Get the name of the "deleted at" column.
     *
     * @return string
     */
    public function getArchivedAtColumn()
    {
        return defined('static::ARCHIVED_AT') ? static::ARCHIVED_AT : 'archived_at';
    }

    /**
     * Get the fully qualified "archived at" column.
     *
     * @return string
     */
    public function getQualifiedArchivedAtColumn()
    {
        return $this->getTable().'.'.$this->getDeletedAtColumn();
    }
}