<?php

namespace Snapdesign\Laravel\Utilities\Providers;

use Illuminate\Support\ServiceProvider;

class CorsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Publish of configuration
        $this->publishes([
            __DIR__.'/../../config/cors.php' => config_path('cors.php'),
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
