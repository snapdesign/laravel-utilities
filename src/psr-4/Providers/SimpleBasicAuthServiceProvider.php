<?php

namespace Snapdesign\Laravel\Utilities\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Snapdesign\Laravel\Utilities\Http\Middleware\SimpleBasicAuth;

class SimpleBasicAuthServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @param Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        // Publish of configuration
        $this->publishes([
            __DIR__.'/../../config/simple_basic_auth.php' => config_path('simple_basic_auth.php'),
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
