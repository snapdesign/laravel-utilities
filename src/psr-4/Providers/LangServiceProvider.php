<?php

namespace Snapdesign\Laravel\Utilities\Providers;

use Illuminate\Support\ServiceProvider;

class LangServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Publish languages
        $this->publishes([
            __DIR__ . '/../../resources/lang/' => resource_path('lang/'),
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
