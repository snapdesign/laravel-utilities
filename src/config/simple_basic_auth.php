<?php

return [
    /*
     |--------------------------------------------------------------------------
     | Simple Basic Auth
     |--------------------------------------------------------------------------
     |
     | Basic Auth Username and Password
     |
     */
    'user' => env('BASIC_AUTH_USER', 'admin'),
    'password' => env('BASIC_AUTH_Password')
];
