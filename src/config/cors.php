<?php

return [
    /*
     |--------------------------------------------------------------------------
     | Laravel CORS
     |--------------------------------------------------------------------------
     |
     | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
     | to accept any value.
     |
     */

    'allowedOrigins' => '*',
    'allowedMethods' => 'GET, POST, PUT, PATCH, DELETE, OPTIONS',
    'allowedHeaders' => 'Origin, Content-Type, Accept, Authorization, X-Request-With',
    'allowCredentials' => true,
];